import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  { id: 1, name: 'frenchfries', price: 10 },
  { id: 2, name: 'Chocolate', price: 20 },
  { id: 3, name: 'pancake1', price: 30 },
];
let lastProductID = 4;
@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductID++,
      ...createProductDto,
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
    return `This action updates a #${id} product`;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedProduct = products[index];
    products.splice(index, 1);
    return deletedProduct;
  }
  reset() {
    products = [
      { id: 1, name: 'frenchfries', price: 10 },
      { id: 2, name: 'Chocolate', price: 20 },
      { id: 3, name: 'pancake1', price: 30 },
    ];
    lastProductID = 4;
    return 'RESET';
  }
}
